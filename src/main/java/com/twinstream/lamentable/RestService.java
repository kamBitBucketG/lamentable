package com.twinstream.lamentable;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

@RestController
public class RestService {

    @PostMapping(path = "/upload-file", consumes = {"application/octet-stream"}, produces = {"plain/text"})
    public ResponseEntity<Void> upload(@RequestParam String username, @RequestParam String filename, final InputStream inputStream) throws
        IOException {

        registerUpload(username, filename);
        saveFile(filename, inputStream);

        final ResponseEntity response = ResponseEntity.ok().build();
        return response;
    }

    @GetMapping(path = "/entries", produces = {"text/csv"})
    public ResponseEntity<String> getEntries() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(getEntriesFile()));
        StringBuffer buffer = new StringBuffer();
        String nextLine;
        while((nextLine = reader.readLine()) != null) {
            buffer.append(nextLine);
            buffer.append("\n");
        }
        String result = buffer.toString();
        return ResponseEntity.ok(result);
    }

    private File getEntriesFile() throws IOException {
        File entries = new File(System.getProperty("user.home") + "/entries.txt");
        if(!entries.exists()) {
            entries.createNewFile();
        }
        return entries;
    }

    private void registerUpload(String username,  String filename) throws IOException {
        String date = new Date().toString();
        String entry = String.format("%s,%s,%s",  date, username, filename);
        File entries = getEntriesFile();
        FileWriter writer = new FileWriter(entries, true);
        writer.append(entry + "\n");
        writer.flush();
    }

    private void saveFile(final String filename, final InputStream inputStream) throws IOException {
        File directory = new File(System.getProperty("user.home") + "/uploads");
        if(!directory.exists()) {
            directory.mkdir();
        }

        File file = new File(directory.getPath() + "/" + filename);

        FileWriter writer = new FileWriter(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuffer buffer = new StringBuffer();
        String nextLine;
        while((nextLine = reader.readLine()) != null) {
            buffer.append(nextLine);
            buffer.append("\n");
        }
        String content = buffer.toString();
        writer.append(content);
        writer.flush();
    }
}
