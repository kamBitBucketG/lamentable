#!/bin/bash
rm -f ~/entries.txt
rm -rf ~/uploads

curl --location --request POST "localhost:8019/upload-file?username=John&filename=example1.txt" \
  --header "Content-Type: application/octet-stream" \
  --data-binary example1.txt

curl --location --request POST "localhost:8019/upload-file?username=Fred&filename=example2.txt" \
  --header "Content-Type: application/octet-stream" \
  --data-binary example2.txt

curl --location --request GET "localhost:8019/entries"
